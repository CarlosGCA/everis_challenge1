from flask import Flask, jsonify, request
import socket
import json

app = Flask(__name__)

def esFloat(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False

def tieneFormato(j):
    try: 
        return ('number' in json.loads(j))
    except ValueError:
        return False

@app.route('/greetings')
def greetingFromHostname():
    return 'Hello World from ' + socket.gethostname()

@app.route('/square/<string:number>')
def squareGet(number):
    if (esFloat(number)):
        return str(float(number) * float(number))
    return "El parámetro enviado debe ser un número"

@app.route('/square', methods=['POST'])
def squarePost():
    rdata = request.data
    if(not tieneFormato(rdata)):
        return jsonify({"message":"El número debe enviarse en formato JSON dentro de un objeto nombrado 'number'"}), 400
    
    number = json.loads(rdata)["number"]
    if (esFloat(number)):
        return jsonify({"square":float(number) * float(number)})
    return jsonify({"message":"El parámetro enviado debe ser un número"}), 400

if __name__=='__main__':
    app.run(host="0.0.0.0", debug=True, port=5000)