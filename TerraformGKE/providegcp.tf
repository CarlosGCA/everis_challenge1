provider "google" {
  version = "3.5.0"

  credentials = file("${var.credentials_file}")

  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_container_cluster" "my_cluster" {
  name = "my-cluster"
  
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "my_primary_node_pool" {
  name       = "my-node-pool"
  cluster    = google_container_cluster.my_cluster.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
    ]
  }

  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${google_container_cluster.my_cluster.name} --region ${var.zone}"
  }

  provisioner "local-exec" {
    command = "kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/cloud/deploy.yaml"
  }
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.my_cluster.name
  description = "GKE Cluster Name"
}