#!/bin/bash
set -e
source variables.txt
export $(cut -d= -f1 variables.txt)
gcloud auth activate-service-account --project=$TF_VAR_project --key-file=./$TF_VAR_credentials_file
gcloud services enable containerregistry.googleapis.com || echo "No se pudo habilitar la Google Container Registry API. Por favor verificar que este habilitada"
gcloud services enable container.googleapis.com || echo "No se pudo habilitar la Kubernetes Engine API. Por favor verificar que este habilitada"
echo "¿Que acción desea realizar? (CREATE, DESTROY)"
read resp
if [ "$resp" == "CREATE" ] 
then
docker build -t gcr.io/$TF_VAR_project/rest-python-app ./PythonRestApp/
docker login -u _json_key --password-stdin https://gcr.io < ./$TF_VAR_credentials_file
docker push gcr.io/$TF_VAR_project/rest-python-app
terraform init ./TerraformGKE
terraform apply -auto-approve ./TerraformGKE
envsubst < ./Kubernetes/app-deployment.yaml | kubectl apply -f -
kubectl apply -f ./Kubernetes/loadbalancer.yaml
export PU_IP=$(kubectl get svc python-rest-loadbalancer -o yaml | grep ip)
echo "Cargando IP pública ..."
while [ -z "$PU_IP" ]
do
  sleep 5
  export PU_IP=$(kubectl get svc python-rest-loadbalancer -o yaml | grep ip)
done
echo $PU_IP
echo "Servicios:"
echo "GET  $PU_IP/greetings"
echo "GET  $PU_IP/square/<number>"
echo "POST $PU_IP/square"
elif [ "$resp" == "DESTROY" ]
then
terraform destroy -auto-approve ./TerraformGKE
else
echo "Las únicas respuestas aceptadas son CREATE o DESTROY"
fi
echo "Fin"
