# Everis_Challenge1
Los prerequisitos para iniciar el proyecto es tener instalados correctamente los siguientes comandos en el entorno ejecutor:
- terraform (<=v0.13.2)
- docker
- gcloud
- kubectl

SOLO PARA LINUX CENTOS: Es posible instalar todos los prerequisitos del proyecto ejecutando el archivo installDependencies.sh
En el caso de Windows adicionalmente es necesario contar con Git Bash para ejecutar el script del proyecto.

Tambien es necesario contar con las APIS Google Container Registry y Kubernetes Engine habilitadas en el proyecto.
La activación de estas APIS está incluida en el script de ejecución pero para esto el Service Account utilizado deberá contar con los permisos correspondientes. Sino tiene los permisos para activarla se proseguira con los siguientes pasos.

Una vez se cuente con los prerequisitos, los siguientes pasos obligatorios son:
1. Crear un Service Account, bajar las credenciales en formato json y copiar el archivo en la carpeta del proyecto (misma carpeta que el presente README).
2. Llenar el archivo variables.txt con las datos correspondientes de nombre de proyecto, región, zona y nombre del archivo de credenciales json que fue incluido en esta carpeta en el paso anterior. Es importante no dejar ningun espacio entre el '=' y el fin de linea.

Finalmente, ejecutar el archivo start.sh como administrador o root. 

Luego de la creación de la infraestructura se mostrará como output los servicios disponibles:
GET		/greetings
GET 	/square/<number>	Sustituir <number> con un número cualquiera
POST	/square				Ejemplo Body envio: {"number":4}

# Información del proyecto
/TerraformGKE : Carpeta con los archivos tf que generan la infraestructura para el GKE e instala un ingress Nginx controller dentro del primero.
/PythonRestApp : Carpeta con el código python de la aplicación REST y el dockerfile que generá una imagen a travéz del primero.
/Kubernetes: Carpeta con los archivos yml utilizados para levantar el contenedor en el GKE y permitir el acceso público al servicio mediante un balanceador de carga. 